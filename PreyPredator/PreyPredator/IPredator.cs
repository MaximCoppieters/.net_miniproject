﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreyPredator
{
    public interface IPredator
    {
        Position Position
        {
            get;
        }

        List<IPrey> Chase(List<IPrey> prey);
        void Eat(List<IPrey> prey);
        int Distance(IPrey prey);
        IPredator Breed();
        bool Starving
        {
            get;
        }
    }
}
