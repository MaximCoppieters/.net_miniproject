﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PreyPredator
{
    public abstract class Insect : IDisplayable
    {
        private Position position;
        private int age;
        private const int INSECT_RADIUS = 10;
        private Ellipse body;
        private double xScale = 200/16;
        private double yScale = 200/16;
        protected static Random movementGenerator = new Random();
        protected static int MOVE_COUNT = 4;
        
        public Insect(Position position)
        {
            this.position = position;
            body = new Ellipse();
        }

        public void MoveForward()
        {
            position.YCoord += 1;
        }

        public void MoveBack()
        {
            position.YCoord -= 1;
        }

        public void MoveRight()
        {
            position.XCoord += 1;
        }

        public void MoveLeft()
        {
            position.XCoord -= 1;
        }

        protected void MoveInRandomDirection()
        {
            switch(movementGenerator.Next(MOVE_COUNT))
            {
                case 0: MoveForward();
                    break;
                case 1: MoveRight();
                    break;
                case 2: MoveBack();
                    break;
                case 3: MoveLeft();
                    break;
            }
        }

        public void DisplayOn(Canvas canvas, Color color)
        {
            SolidColorBrush brush = new SolidColorBrush(color);
            xScale = canvas.Width / 16;
            yScale = canvas.Height / 16;
            body.Stroke = brush;
            body.Fill = brush;
            body.Width = INSECT_RADIUS;
            body.Height = INSECT_RADIUS;
            body.Margin = new Thickness((position.XCoord * xScale) + body.Width / 2 , (position.YCoord * yScale) + body.Height / 2,0,0);
            canvas.Children.Add(body);
        }

        public void RemoveFromCanvas()
        {
            body.Visibility = Visibility.Collapsed;
        }

        public void UpdateDisplay()
        {
            body.Margin = new Thickness((position.XCoord * xScale) + body.Width / 2 , (position.YCoord * yScale) + body.Height / 2,0,0);
        }

        public int Age {
            get { return age; }
            set { age = value; }
        }

        public Position Position
        {
            get { return position; }
            set { position = value; }
        }
    }
}
