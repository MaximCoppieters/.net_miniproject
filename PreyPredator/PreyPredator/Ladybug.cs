﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PreyPredator
{
    public class Ladybug : Insect, IPredator
    {
        private const int BREEDING_AGE = 6;
        private const int CATCHING_LIMIT = 20;
        private const int STARVATION_TIME = 3;
        private bool starving;
        private int roundsNotEaten;

        public Ladybug(Position position)
            :base(position)
        {

        }

        public int XCoord
        {
            get { return Position.XCoord; }
        }

        public int YCoord
        {
            get { return Position.YCoord; }
        }

        public bool Starving
        {
            get { return starving; }
        }

        public IPredator PassRound(List<IPrey> preyAvailable)
        {
            Age += 1;

            MoveInRandomDirection();
            List<IPrey> caughtPrey = Chase(preyAvailable);
            Eat(caughtPrey);
            UpdateDisplay();
            if(Age % BREEDING_AGE == 0)
            {
                return Breed();
            }
            return null;
        }


        public IPredator Breed()
        {
            return new Ladybug(new Position(Position.XCoord, Position.YCoord));
        }

        public List<IPrey> Chase(List<IPrey> prey)
        {
            List<IPrey> caughtPrey = new List<IPrey>();
            IPrey current;

            for(int i=0; i < prey.Count && caughtPrey.Count < CATCHING_LIMIT; i++)
            {
                current = prey[i];
                if(Distance(current) <= 2)
                {
                    caughtPrey.Add(prey[i]);
                }
            }

            return caughtPrey;
        }

        public int Distance(IPrey prey)
        {
            return (int)Math.Sqrt(Math.Pow(prey.Position.XCoord - XCoord, 2) + Math.Pow(prey.Position.YCoord - YCoord, 2));
        }

        public void Eat(List<IPrey> prey)
        {
            if(prey.Count > 0)
            {
                roundsNotEaten = 0;
            }
        }
    }
}
