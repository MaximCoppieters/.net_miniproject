﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace PreyPredator
{
    public interface IDisplayable
    {
        void DisplayOn(Canvas canvas, Color color);
        void RemoveFromCanvas();
        void UpdateDisplay();
    }
}
