﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreyPredator
{
    public class Louse : Insect, IPrey
    {
        private const int BREEDING_AGE = 5;

        public Louse(Position position)
            :base(position)
        {

        }

        public IPrey PassRound()
        {
            this.Age += 1;
            MoveInRandomDirection();
            UpdateDisplay();
            if(Age % BREEDING_AGE == 0)
            {
                return Breed();
            }
            return null;
        }

        public IPrey Breed()
        {
            return new Louse(base.Position);
        }
    }
}
