﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PreyPredator
{
    public class Position
    {
        private int xCoord;
        private int yCoord;

        public Position(int xCoord, int yCoord)
        {
            this.xCoord = checkIfCoordCorrect(xCoord);
            this.yCoord = checkIfCoordCorrect(yCoord);
        }

        private int checkIfCoordCorrect(int coord)
        {
            if(coord < 0)
            {
                return 0;
            } else
            {
                if(coord > 15)
                {
                    return 15;
                }
            }
            return coord;
        }

        public int XCoord {
            get { return xCoord; }
            set { xCoord = value; }
        }
        public int YCoord {
            get { return yCoord; }
            set { yCoord = value; }
        }
    }
}
