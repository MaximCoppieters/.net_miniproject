﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreyPredator
{
    public interface IPrey
    {
        Position Position
        {
            get;
        }

        IPrey Breed();
    }
}
