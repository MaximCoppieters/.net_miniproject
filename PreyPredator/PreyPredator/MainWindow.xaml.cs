﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PreyPredator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Random seed = new Random();
        private Random rng;
        private List<IPrey> lice = new List<IPrey>();
        private List<IPredator> ladybugs = new List<IPredator>();
        private Color louseColor = Colors.Green;
        private Color ladybugColor = Colors.Red;
        private int round = 0;

        public MainWindow()
        {
            InitializeComponent();
            rng = new Random(seed.Next());

            GeneratePrey(100, lice);
            GeneratePredators(4, ladybugs);
            DrawInsects(ladybugs, ladybugColor);
            DrawInsects(lice, louseColor);
            DisplayLabels();
        }

        private void DrawInsects<T>(List<T> insects, Color color)
        {
            foreach(object animal in insects)
            {
                ((Insect)animal).DisplayOn(worldCanvas, color);
            }
        }

        public void GeneratePredators(int amount, List<IPredator> predators)
        {
            for (int i = 0; i < amount; i++)
            {
                int x = rng.Next(16);
                int y = rng.Next(16);

                predators.Add(new Ladybug(new Position(x, y)));
            }
        }

        public void GeneratePrey(int amount, List<IPrey> prey)
        {
            for (int i = 0; i < amount; i++)
            {
                int x = rng.Next(16);
                int y = rng.Next(16);

                prey.Add(new Louse(new Position(x, y)));
            }
        }

        private void nextRoundButton_Click(object sender, RoutedEventArgs e)
        {
            List<IPredator> newPredators = new List<IPredator>();
            foreach(IPredator predator in ladybugs)
            {
                Ladybug ladybug = (Ladybug)predator;
                IPredator newLadyBug = ladybug.PassRound(lice);
                if(!ladybug.Starving)
                {
                    newPredators.Add(ladybug);
                }
                if(newLadyBug != null)
                {
                    ((Insect)newLadyBug).DisplayOn(worldCanvas, ladybugColor);
                    newPredators.Add(newLadyBug);
                }
            }
            ladybugs = newPredators;

            List<IPrey> newPrey = new List<IPrey>();
            foreach(IPrey prey in lice)
            {
                if(prey != null)
                {
                    Louse louse = (Louse)prey;
                    IPrey newLouse = louse.PassRound();
                    newPrey.Add(louse);
                    if(newLouse != null)
                    {
                        ((Insect)newLouse).DisplayOn(worldCanvas, louseColor);
                        newPrey.Add(newLouse);
                    }
                }
            }
            lice = newPrey;

            round++;
            DisplayLabels();
        }

        public void DisplayLabels()
        {
            ladybugLabel.Content = ladybugs.Count;
            liceLabel.Content = lice.Count;
            roundLabel.Content = round;
        }
    }
}
