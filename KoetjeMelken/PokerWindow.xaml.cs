﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KoetjeMelken
{
    /// <summary>
    /// Interaction logic for PokerWindow.xaml
    /// </summary>
    public partial class PokerWindow : Window
    {
        private Deck computerDeck;
        private Deck playerDeck;
        private DispatcherTimer actionTimer = new DispatcherTimer();
        private Card currentComputerCard;
        private Card currentPlayerCard;
        private List<Card> playerHand;
        private List<Card> computerHand;

        private static String[] handStrengthDescriptions = 
            { "High Card", "Pair", "Two Pairs", "Three of a Kind", "Straight",
            "Flush", "Full House", "Four of a Kind", "Straight Flush"};

        private double playerHandStrength;
        private double computerHandStrength;
        private int playerWinCount;
        private int computerWinCount;
        private int gameCount;

        public PokerWindow(string[] suits, string[] rankings)
        {
            InitializeComponent();

            computerDeck = new Deck(rankings, suits);
            playerDeck = new Deck(rankings, suits);
        }

        private void exitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void ActionTimer_Tick(object sender, EventArgs e)
        {
            ClearFields();

            if(computerDeck.CardCount >= 5)
            {
                computerHand = GetHand(computerDeck);

                DrawHandOnGrid(computerCardGrid, computerHand);

                computerHandStrength = EvaluateHand(computerHand);

                ShowHandStrength(computerHandLabel, computerHandStrength);

                ToggleCardDealing();
            } else
            {
                FinishGame();
            }

            actionTimer.Stop();
        }

        private void FinishGame()
        {
            if (computerWinCount > playerWinCount)
            {
                gameResultTextBlock.Text = $"Sorry, you lost. " +
                    $"The computer won {computerWinCount} times, you won {playerWinCount} times";
            }
            else
            {
                if (computerWinCount < playerWinCount)
                {
                    gameResultTextBlock.Text = $"Congratulations!! You are the winner!!" +
                        $" The computer won {computerWinCount} times, you won {playerWinCount} times";
                }
                else
                {
                    gameResultTextBlock.Text = $"You have the same result as the computer: {playerWinCount}";
                }
            }
        }

        private void DrawHandOnGrid(Grid grid, List<Card> hand)
        {
            int handIndex = 0;

            foreach (Canvas canvas in grid.Children)
            {
                currentComputerCard = hand[handIndex++];
                currentComputerCard.DrawOnCanvas(canvas);
            }
        }

        private List<Card> GetHand(Deck deck)
        {
            List<Card> hand = new List<Card>();

            for (int i = 0; i < 5; i++)
            {
                hand.Add(deck.Pop());
            }

            return hand;
        }

        private void ToggleCardDealing()
        {
            dealButton.IsEnabled = !dealButton.IsEnabled;
        }

        private void InitializeGame()
        {
            actionTimer.Interval = TimeSpan.FromSeconds(2);
            actionTimer.Tick += ActionTimer_Tick;
        }

        private void ShowHandStrength(Label label, double handStrength)
        {
            label.Content = handStrengthDescriptions[(int)handStrength];
        }

        private void shuffleButton_Click(object sender, RoutedEventArgs e)
        {
            computerDeck.Shuffle();
            playerDeck.Shuffle();

            shuffledLabel.Content = "DECKS ARE SHUFFLED";
            InitializeGame();

            actionTimer.Start();
        }

        private void dealCard_Click(object sender, RoutedEventArgs e)
        {

            List<Card> playerHand = GetHand(playerDeck);

            DrawHandOnGrid(playerCardGrid, playerHand);

            playerHandStrength = EvaluateHand(playerHand);

            ShowHandStrength(playerHandLabel, playerHandStrength);
            CompareHandStrength();

            ToggleCardDealing();
            actionTimer.Start();
        }

        private void CompareHandStrength()
        {
            Console.WriteLine(computerHandStrength + " vs " + playerHandStrength);

            if(computerHandStrength > playerHandStrength)
            {
                computerWinCount++;
                computerResultLabel.Content = "Computer wins";
            } else
            {
                if(computerHandStrength < playerHandStrength)
                {
                    playerWinCount++;
                    playerResultLabel.Content = "Player wins";
                } else
                {
                    if(computerHandStrength == playerHandStrength)
                    {
                        playerResultLabel.Content = "Equal";
                        computerResultLabel.Content = "Equal";
                    }
                }
            }
            gameCount++;
        }

        private void ClearFields()
        {
            playerWinsLabel.Content = string.Empty;
            computerWinsLabel.Content = string.Empty;
            shuffledLabel.Content = string.Empty;
            playerHandLabel.Content = string.Empty;
            computerHandLabel.Content = string.Empty;
            computerResultLabel.Content = string.Empty;
            playerResultLabel.Content = string.Empty;

            clearCanvasesInGrid(playerCardGrid);
            clearCanvasesInGrid(computerCardGrid);
        }

        private void clearCanvasesInGrid(Grid grid)
        {
            foreach (Canvas canvas in grid.Children)
            {
                canvas.Children.Clear();
            }
        }

        private double EvaluateHand(List<Card> hand)
        {
            Dictionary<int, int> occurencesByRanking = new Dictionary<int, int>();

            int highCardValue = 0;

            hand.Sort();

            bool flush = true;
            bool straight = true;
            Card last = null;
            foreach (Card card in hand)
            {
                highCardValue = Math.Max(highCardValue, card.Value);

                if (!occurencesByRanking.ContainsKey(card.Value))
                {
                    occurencesByRanking.Add(card.Value, 0);
                }
                occurencesByRanking[card.Value]++;

                if (last == null)
                {
                    last = card;
                } else
                {
                    if (last.Suit != card.Suit)
                    {
                        flush = false;
                    }
                    if (card.Value - last.Value != 0)
                    {
                        straight = false;
                    }
                }
                last = card;
            }

            return DecideHandStrength(occurencesByRanking, flush, straight);

        }


        /*  De waarde van een hand wordt aangegeven door een komma getal
         *  Getal voor de komma geeft aan van welke rang de hand is (0->7 of High Card -> Straight Flush)
         *  Het getal na de komma geeft aan wat de waarde van de kaart is in geval dat beide spelers
         *  een hand trekken van dezelfde rang.
         *  Bijv: High Card 8 < High Card Ace
         *  -> 0.08 vs 0.12
         */
        private double DecideHandStrength(Dictionary<int,int> occurencesByRanking, bool flush, bool straight)
        {
            if (flush && straight)
                return 7 + 0.01 * occurencesByRanking.Keys.Max();
            else
            if (occurencesByRanking.ContainsValue(4))
               return 6 + 0.01 * GetCardLevelOfOccurence(occurencesByRanking, 4);
            else
                if (occurencesByRanking.ContainsValue(3) && occurencesByRanking.ContainsValue(2))
                    return 5 + 0.01 * GetCardLevelOfOccurence(occurencesByRanking, 3);
            else
                if (flush)
                    return 4 + 0.01 * occurencesByRanking.Keys.Max();
            else
                if (straight)
                    return 3 + 0.01 * occurencesByRanking.Keys.Max();
            else
                if (ContainsMultiplePairs(occurencesByRanking))
                    return 2 + 0.01 * GetCardLevelOfOccurence(occurencesByRanking, 2);
                else
                if (occurencesByRanking.ContainsValue(2))
                    return 1 + 0.01 * GetCardLevelOfOccurence(occurencesByRanking,2);

            return 0 + 0.01 * occurencesByRanking.Keys.Max();
        }

        private int GetCardLevelOfOccurence(Dictionary<int, int> occurencesByRanking, int occurences)
        {
            foreach(int cardRank in occurencesByRanking.Keys)
            {
                if(occurences == occurencesByRanking[cardRank])
                {
                    return cardRank;
                }
            }
            throw new InvalidOperationException("Occurences not present in dictionary");
        }

        private bool ContainsMultiplePairs(Dictionary<int, int> occurencesByRanking)
        {
            bool containsMultiplePairs = false;

            foreach(int value in occurencesByRanking.Values)
            {
                if(value == 2)
                {
                    if(!containsMultiplePairs)
                    {
                        containsMultiplePairs = true;
                    } else
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
