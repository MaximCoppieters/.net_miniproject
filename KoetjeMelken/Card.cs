﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace KoetjeMelken
{
    public class Card : IComparable
    {
        private string suit;
        private string rank;
        private int value;

        public Card(string suit, string rank, int value)
        {
            this.suit = suit;
            this.rank = rank;
            this.value = value;
        }

        public string Suit
        {
            get
            { return suit; }
        }

        public string Rank
        {
            get
            { return rank; }
        }

        public int Value
        {
            get
            { return value; }
        }

        public int CompareTo(object obj)
        {
            Card card = (Card)obj;
            if(this.value > card.value)
            {
                return 1;
            } else
            {
                if (this.value == card.value)
                {
                    return 0;
                } else
                {
                    return -1;
                }
            }
        }

        public void DrawOnCanvas(Canvas cardCanvas)
        {
            string imageFileName = Rank.ToString() + Suit.ToString() + ".png";
            BitmapImage cardBitmap = DrawUtil.GetBitmapImageFromUri(new Uri(@"..\..\Images\" + imageFileName, UriKind.Relative));

            DrawUtil.DrawBitmapImage(cardCanvas, cardBitmap);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if(obj is Card)
            {
                Card card = (Card)obj;
                if (card.Suit == suit && card.Rank == rank) return true;
            }
            return false;
        }
    }
}
