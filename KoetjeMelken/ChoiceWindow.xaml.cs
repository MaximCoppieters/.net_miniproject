﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KoetjeMelken
{
    /// <summary>
    /// Interaction logic for ChoiceWindow.xaml
    /// </summary>
    public partial class ChoiceWindow : Window
    {
        private Deck playerDeck;
        private Deck computerDeck;
        private string[] rankings;
        private string[] suits;

        public ChoiceWindow()
        {
            InitializeComponent();
        }

        private void openMenuItem_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog openFileDialog = new OpenFileDialog();
            string currentFolder = System.IO.Path.GetFullPath(System.IO.Path.Combine(Directory.GetCurrentDirectory(), @"..\..\")); 
            openFileDialog.InitialDirectory = currentFolder;
            openFileDialog.Filter = "Text Files|*.txt";

            if (openFileDialog.ShowDialog() == true) // User clicks Open
            {

                string textFile = openFileDialog.FileName;
                currentFolder = Directory.GetParent(textFile).ToString();

                StreamReader textReader = null;
                try
                {
                    FileStream cardInputStream = new FileStream(textFile, FileMode.Open, FileAccess.Read);

                    textReader = new StreamReader(cardInputStream);

                    rankings = textReader.ReadLine().Split(',');
                    suits = textReader.ReadLine().Split(',');

                    playerDeck = new Deck(rankings, suits);
                    computerDeck = new Deck(rankings, suits);


                    choiceStackPanel.IsEnabled = true;
                    koetjeRadioButton.IsChecked = true;
                    startButton.IsEnabled = true;
                }
                catch (FileNotFoundException ex)
                {
                    MessageBox.Show("Fout formaat of niet gevonden!");
                }
                finally
                {
                    if (textReader != null)
                    {
                        textReader.Close();
                    }
                }

            }

        }

        private void exitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            Window toOpen;

            if(hogerLagerRadioButton.IsChecked ?? true)
            {
                toOpen = new HigherLowerSetupWindow(suits, rankings);
            } else
            {
                if(koetjeRadioButton.IsChecked ?? true)
                {
                    toOpen = new KoetjeWindow(playerDeck, computerDeck);
                } else
                {
                    toOpen = new PokerWindow(suits, rankings);
                }
            }

            this.Close();
            toOpen.Show();
        }
    }
}
