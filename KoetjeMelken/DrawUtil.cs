﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Controls;

namespace KoetjeMelken
{
    public class DrawUtil
    {
        private static Uri debugFolder = new Uri(@AppDomain.CurrentDomain.BaseDirectory, UriKind.Absolute);

        public static BitmapImage GetBitmapImageFromUri(Uri imageUri)
        {
            BitmapImage bitmapImage = new BitmapImage();
            
            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri(debugFolder, imageUri);
            bitmapImage.EndInit();

            return bitmapImage;
        }

        public static void DrawBitmapImage(Canvas targetCanvas, BitmapImage toDraw)
        {
            Image picture = new Image();

            picture.Source = toDraw;
            picture.Width = targetCanvas.Width;
            picture.Height = targetCanvas.Height;

            targetCanvas.Children.Add(picture);
        }

        public static void DrawImage(Canvas targetCanvas, string imageName)
        {
            BitmapImage image = GetBitmapImageFromUri(new Uri(@"..\..\Images\" + imageName, UriKind.Relative));

            DrawBitmapImage(targetCanvas, image);
        }

        public static void DrawImageOnButton(Button target, string imageName)
        {
            BitmapImage image = GetBitmapImageFromUri(new Uri(@"..\..\Images\" + imageName, UriKind.Relative));

            DrawBitmapImageOnButton(target, image);
        }

        public static void DrawBitmapImageOnButton(Button target, BitmapImage toDraw)
        {
            Image picture = new Image();

            picture.Source = toDraw;
            picture.Width = target.Width;
            picture.Height = target.Height;

            target.Content = picture;
        }
    }
}
