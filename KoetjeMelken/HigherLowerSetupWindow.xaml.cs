﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KoetjeMelken
{
    /// <summary>
    /// Interaction logic for HigherLowerWindow.xaml
    /// </summary>
    public partial class HigherLowerSetupWindow : Window
    {
        private Deck playDeck;
        private Card startingCard;
        private string[] rankings;
        private string[] suits;

        public HigherLowerSetupWindow(string[] suits, string[] rankings)
        {
            InitializeComponent();
            playDeck = new Deck(rankings, suits);
            valueSlider.Maximum = rankings.Length-1;
            valueSlider.TickFrequency = 1;
            valueSlider.IsSnapToTickEnabled = true;

            this.rankings = rankings;
            this.suits = suits;

            PopulateComboBox(suits);
            UpdateCanvas();

            valueSlider.ValueChanged += ValueSlider_ValueChanged;
            suitComboBox.SelectionChanged += SuitComboBox_SelectionChanged;

            valueSlider.Value = 0;
        }

        private void SuitComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateCanvas();
        }

        private void ValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            UpdateCanvas();
        }

        private void UpdateCanvas()
        {
            ComboBoxItem currentItem = (ComboBoxItem)suitComboBox.SelectedItem;

            startingCard = playDeck.findCardOfSuitAndRank(Convert.ToString(currentItem.Content), rankings[(int)valueSlider.Value]);
            startingCard.DrawOnCanvas(cardCanvas);
        }

        private void PopulateComboBox(string[] suits)
        {
            foreach(string suit in suits)
            {
                ComboBoxItem currentSuit = new ComboBoxItem();
                currentSuit.Content = suit;
                suitComboBox.Items.Add(currentSuit);
            }

            if(suitComboBox.HasItems)
            {
                suitComboBox.SelectedIndex = 0;
            }
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int amount = Convert.ToInt32(amountTextBox.Text);

                if (amount < 2 || amount > playDeck.CardCount)
                {
                    MessageBox.Show("Amount not valid");
                    return;
                }
                Window higherLowerGameWindow = new HigherLowerPlayWindow(startingCard, amount, playDeck, this);
                this.Hide();
                higherLowerGameWindow.Show();
            }
            catch (FormatException fe)
            {
                MessageBox.Show("Geef een aantal kaarten in");
            }
        }
    }
}
