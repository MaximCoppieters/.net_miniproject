﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KoetjeMelken
{
    /// <summary>
    /// Interaction logic for HigherLowerPlayWindow.xaml
    /// </summary>
    public partial class HigherLowerPlayWindow : Window
    {
        List<Card> cardsInPlay;
        Card currentCard;
        Window callingWindow;
        int cardIndex = 0;

        public HigherLowerPlayWindow(Card startingCard, int amount, Deck playDeck, Window callingWindow)
        {
            InitializeComponent();
            DrawUtil.DrawImageOnButton(higherButton, "up.png");
            DrawUtil.DrawImageOnButton(lowerButton, "down.png");

            currentCard = startingCard;
            this.callingWindow = callingWindow;

            GetAmountOfCards(amount, playDeck);
            DrawCardsOnCanvas(cardsInPlay);
        }

        private void DrawCardsOnCanvas(List<Card> cardsInPlay)
        {
            int distanceBetweenCards = CalculateDistanceBetweenCards();
            int currentDistance = 0;

            Canvas currentCanvas = CreateCanvasAtDistance(currentDistance);
            currentCard.DrawOnCanvas(currentCanvas);

            canvasGrid.Children.Add(currentCanvas);

            currentDistance += distanceBetweenCards;

            for(int i=1; i < cardsInPlay.Count; i++)
            {
                currentCanvas = CreateCanvasAtDistance(currentDistance);
                DrawUtil.DrawImage(currentCanvas, "cardback.png");
                canvasGrid.Children.Add(currentCanvas);

                currentDistance += distanceBetweenCards;
            }
        }

        private Canvas CreateCanvasAtDistance(int distance)
        {
            Canvas canvas = new Canvas();
            canvas.Width = 100;
            canvas.Height = 150;

            canvas.Margin = new Thickness(distance, 0, 0, 0);

            canvas.HorizontalAlignment = HorizontalAlignment.Left;
            canvas.VerticalAlignment = VerticalAlignment.Top;

            return canvas;
        }
       

        private int CalculateDistanceBetweenCards()
        {
            return (int)(this.Width - 200) / cardsInPlay.Count;
        }

        private void GetAmountOfCards(int amount, Deck playDeck)
        {
            cardsInPlay = new List<Card>();
            playDeck.Shuffle();

            for(int i=0; i < amount; i++)
            {
                Card drawn = playDeck.Pop();
                if (drawn.Equals(currentCard)) continue;
                cardsInPlay.Add(drawn);
            }
        }

        private void higherButton_Click(object sender, RoutedEventArgs e)
        {
            Card compareCard = cardsInPlay[++cardIndex];

            if(compareCard.Value > currentCard.Value)
            {
                FlipNext();
                if(cardIndex >= cardsInPlay.Count)
                {
                    Win();
                }
            } else
            {
                FlipNext();
                Lose();
            }
        }

        private void lowerButton_Click(object sender, RoutedEventArgs e)
        {

            Card compareCard = cardsInPlay[++cardIndex];

            if(compareCard.Value < currentCard.Value)
            {
                FlipNext();
                if(cardIndex >= cardsInPlay.Count)
                {
                    Win();
                }
            } else
            {
                FlipNext();
                Lose();
            }
        }

        private void Lose()
        {
            MessageBox.Show("Jammer, volgende keer beter");
            ReturnToCallingWindow();
        }

        private void Win()
        {
            MessageBox.Show("You win!");
            ReturnToCallingWindow();
        }

        private void ReturnToCallingWindow()
        {
            this.Close();
            callingWindow.Show();
        }

        private void FlipNext()
        {
            currentCard = cardsInPlay[cardIndex];
            currentCard.DrawOnCanvas((Canvas)canvasGrid.Children[cardIndex]);
        }
    }
}
