﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KoetjeMelken
{
    public class Deck
    {
        private Card[] cards;
        private int index;
        private int cardCount;
        private static Random seedGenerator = new Random();

        public Deck(string[] rankings, string[] suits) {
            cards = new Card[rankings.Length * suits.Length];
            foreach(string suit in suits)
            {
                int value = 2;
                foreach(string ranking in rankings)
                {
                    Push(new Card(suit, ranking, value++));
                }
            }
        }

        public void Push(Card cardToAdd)
        {
            if (cardToAdd == null) throw new InvalidOperationException();

            cards[index++] = cardToAdd;
            cardCount++;
        }

        public Card Pop()
        {
            if (IsEmpty()) throw new InvalidOperationException("Deck is empty!");

            --cardCount;
            Console.WriteLine(Convert.ToString(cardCount));
            return cards[--index];
        }

        public bool IsEmpty()
        {
            return cardCount == 0;
        }

        public void Shuffle()
        {
            Random randomGenerator = new Random(seedGenerator.Next());

            int randomIndexToExchange;
            for(int i=1; i < cardCount; i++)
            {
                randomIndexToExchange = randomGenerator.Next(i);
                exchange(i, randomIndexToExchange);
            }
        }

        public Card findCardOfSuitAndRank(string suit, string ranking)
        {
            foreach(Card card in cards) {
                if(card.Suit == suit && card.Rank == ranking)
                {
                    return card;
                }
            }

            throw new KeyNotFoundException();
        }

        private void exchange(int indexLeft, int indexRight)
        {
            Card temp = cards[indexLeft];
            cards[indexLeft] = cards[indexRight];
            cards[indexRight] = temp;
        }

        public int CardCount
        {
            get { return cardCount; }
        }
    }
}
