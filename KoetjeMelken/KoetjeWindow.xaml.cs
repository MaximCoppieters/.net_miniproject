﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KoetjeMelken
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class KoetjeWindow : Window
    {

        private Deck computerDeck;
        private Deck playerDeck;
        private DispatcherTimer actionTimer = new DispatcherTimer();
        private Card currentComputerCard;
        private Card currentPlayerCard;
        private int playerWins;
        private int computerWins;

        public object FileReader { get; private set; }

        public KoetjeWindow(Deck playerDeck, Deck computerDeck)
        {
            this.playerDeck = playerDeck;
            this.computerDeck = computerDeck;
            InitializeComponent();
        }

        private void InitializeGame()
        {
            DrawUtil.DrawImage(playerDeckCanvas, "cardback.png");
            DrawUtil.DrawImage(computerDeckCanvas, "cardback.png");

            actionTimer.Interval = TimeSpan.FromSeconds(2);
            actionTimer.Tick += ActionTimer_Tick;
        }

        private void ActionTimer_Tick(object sender, EventArgs e)
        {
            ClearFields();

            if (!computerDeck.IsEmpty())
            {
                currentComputerCard = computerDeck.Pop();

                DisplayCards(computerDeck, currentComputerCard, computerDeckCanvas, computerCurrentCardCanvas, computerCurrentCardLabel, computerCardCountLabel);
            }

            ToggleCardDealing();

            actionTimer.Stop();
        }

        private void ToggleCardDealing()
        {
            dealButton.IsEnabled = !dealButton.IsEnabled;
            playerDeckCanvas.IsEnabled = !playerDeckCanvas.IsEnabled;
        }

        private void DisplayCards(Deck deck, Card card, Canvas deckCanvas, Canvas cardCanvas, Label currentCardLabel, Label cardCountLabel)
        {
            if(deck.IsEmpty())
            {
                deckCanvas.Children.Clear();
            }

            card.DrawOnCanvas(cardCanvas);

            currentCardLabel.Content = card.Rank + " of " + card.Suit;
            cardCountLabel.Content = "Card #" + (52 - deck.CardCount);
        }

        private void shuffleButton_Click(object sender, RoutedEventArgs e)
        {
            computerDeck.Shuffle();
            playerDeck.Shuffle();

            shuffledLabel.Content = "DECKS ARE SHUFFLED";
            InitializeGame();

            actionTimer.Start();
        }

        private void dealCard_Click(object sender, RoutedEventArgs e)
        {
            if(!playerDeck.IsEmpty())
            {
                currentPlayerCard = playerDeck.Pop();

                DisplayCards(playerDeck, currentPlayerCard, playerDeckCanvas, playerCurrentCardCanvas, playerCurrentCardLabel, playerCardCountLabel);
                CompareCards();

                ToggleCardDealing();
                actionTimer.Start();
            } else
            {
                FinishGame();
            }
        }

        private void FinishGame()
        {
            if(playerWins > computerWins)
            {
                gameResultTextBlock.Text = string.Format("Congratulations!!You are the winner !!The computer won {0}. times, you won {1}. times",
                                            computerWins, playerWins);
            } else
            {
                if(computerWins < playerWins)
                {
                    gameResultTextBlock.Text = string.Format("Sorry, you lost. The computer won {0}. times, you won {1} times",
                                            computerWins, playerWins);
                } else
                {
                    gameResultTextBlock.Text = string.Format("No winner, you have the same result as the computer: you won {0}. times", playerWins);
                }
            }
        }

        private void ClearFields()
        {
            computerCurrentCardLabel.Content = string.Empty;
            playerCurrentCardLabel.Content = string.Empty;
            playerWinsLabel.Content = string.Empty;
            computerWinsLabel.Content = string.Empty;
            shuffledLabel.Content = string.Empty;
        }

        private void CompareCards()
        {
            if(currentComputerCard.Value > currentPlayerCard.Value)
            {
                computerWinsLabel.Content = "Computer wins";
                computerWins++;
            } else
            {
                if(currentComputerCard.Value < currentPlayerCard.Value)
                {
                    playerWinsLabel.Content = "You win";
                    playerWins++;
                } else
                {
                    computerWinsLabel.Content = "Equal";
                    playerWinsLabel.Content = "Equal";
                }
            }
        }

        private void exitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
