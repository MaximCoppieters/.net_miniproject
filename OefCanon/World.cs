﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace OefCanon
{
    public class World
    {
        private const int WORLD_HEIGHT = 120;
        private const int WORLD_WIDTH = 300;
        private const int LINE_HEIGHT = 10;
        private Canvas drawCanvas;
        private SolidColorBrush black = new SolidColorBrush(Colors.Black);

        public World(Canvas drawCanvas)
        {
            this.drawCanvas = drawCanvas;
        }

        public void drawLines()
        {
            Line currentLine;
            TextBlock distanceTextBlock;

            for (int i = 10; i < WORLD_WIDTH; i+=10)
            {
                currentLine = new Line();

                currentLine.X1 = i * getXScale();
                currentLine.X2 = currentLine.X1;
                currentLine.Y1 = drawCanvas.Height;
                currentLine.Y2 = currentLine.Y1 - LINE_HEIGHT;

                currentLine.Stroke = black;
                drawCanvas.Children.Add(currentLine);

                if(i % 100 == 0)
                {
                    distanceTextBlock = new TextBlock();
                    distanceTextBlock.Text = Convert.ToString(i);
                    distanceTextBlock.Margin = new Thickness(currentLine.X1 - 11, currentLine.Y2 - 15, 0, 0);
                    drawCanvas.Children.Add(distanceTextBlock);
                }
            }
        }

        public double getXScale()
        {
            return drawCanvas.Width / WORLD_WIDTH;
        }

        public double getYScale()
        {
            return drawCanvas.Height / WORLD_HEIGHT;
        }
    }
}
