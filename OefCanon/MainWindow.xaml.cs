﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OefCanon
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private World world;
        private CanonBall canonBall;

        public MainWindow()
        {
            InitializeComponent();

            speedSlider.ValueChanged += SpeedSlider_ValueChanged;
            angleSlider.ValueChanged += AngleSlider_ValueChanged;
        }

        private void AngleSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            angleLabel.Content = (int)angleSlider.Value;
        }

        private void SpeedSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            speedLabel.Content = (int)speedSlider.Value;
        }

        private void drawCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            world = new World(drawCanvas);
            canonBall = new CanonBall(drawCanvas.Margin.Left, canonImage.Margin.Top, canonCanvas, distanceLabel, heightLabel);
            double xScale = world.getXScale();
            double yScale = world.getYScale();

            canonBall.XScale = xScale;
            canonBall.YScale = yScale;

            world.drawLines();
        }

        private void shootButton_Click(object sender, RoutedEventArgs e)
        {
            canonBall.Speed = (int)speedSlider.Value;
            canonBall.Angle = (int)angleSlider.Value;

            canonBall.shootCanonBall();
        }

        private void resetButton_Click(object sender, RoutedEventArgs e)
        {
            canonBall = new CanonBall(drawCanvas.Margin.Left, canonImage.Margin.Top, canonCanvas, distanceLabel, heightLabel);
        }
    }
}
