﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace OefCanon
{
    public class CanonBall
    {
        private int radius;
        private SolidColorBrush ballColor;
        private double xPos;
        private double yPos;
        private Ellipse ball;
        private Canvas canonCanvas;
        private DispatcherTimer airTimer;
        private int angle;
        private int speed;
        private double t;
        private const double G = 9.81;
        private double xDistance;
        private double yDistance;
        private Label distanceLabel;
        private Label heightLabel;
        private double xScale;
        private double yScale;

        public CanonBall(double xPos, double yPos, Canvas canonCanvas, Label distanceLabel, Label heightLabel)
        {
            radius = 10;
            ballColor = new SolidColorBrush(Colors.Red);
            this.canonCanvas = canonCanvas;
            this.xPos = xPos - radius / 2;
            this.yPos = yPos - radius / 2;

            this.distanceLabel = distanceLabel;
            this.heightLabel = heightLabel;

            airTimer = new DispatcherTimer();

            airTimer.Interval = TimeSpan.FromMilliseconds(10);
            airTimer.Tick += AirTimer_Tick;
            createCanonBall();
        }

        private void AirTimer_Tick(object sender, EventArgs e)
        {
            t += airTimer.Interval.Milliseconds / 100.0;

            xDistance = speed * Math.Cos(Math.PI / 180 * angle) * t;
            yDistance = speed * Math.Sin(Math.PI / 180 * angle) * t - (1/2 * G) * Math.Pow(t,2);


            if(xPos + xDistance < canonCanvas.Width)
            {
                updateLabels();
                updateCanonBall(xDistance, yDistance);
            } else
            {
                airTimer.Stop();
            }
        }

        public void createCanonBall()
        {
            ball = new Ellipse();
            ball.Stroke = ballColor;
            ball.Fill = ballColor;
            ball.Width = radius * 2;
            ball.Height = radius * 2;
            ball.Margin = new Thickness(xPos, yPos, 0, 0);

            canonCanvas.Children.Add(ball);
        }

        public void shootCanonBall()
        {
            airTimer.Start();
        }

        private void updateLabels()
        {
            heightLabel.Content = (int)(yDistance / yScale);
            distanceLabel.Content = (int)(xDistance / xScale);
        }

        private void updateCanonBall(double xDistance, double yDistance)
        {
            ball.Margin = new Thickness(xPos + xDistance, yPos - yDistance, 0, 0);
        }

        public int Speed
        {
            set { speed = value; }
        }

        public int Angle
        {
            set { angle = value; }
        }

        public double Distance
        {
            get
            {
                return xDistance;
            }
        }

        public double Height
        {
            get
            {
                return yDistance;
            }
        }

        public double XScale
        {
            set
            {
                xScale = value;
            }
        }

        public double YScale
        {
            set
            {
                yScale = value;
            }
        }

    }
}
